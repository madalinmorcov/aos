package com.madalinmorcov.aoslab.persistence.repositories;

import com.madalinmorcov.aoslab.persistence.entities.Person;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.Properties;

public class PersonRepository {

  private static final String PERSISTANCE_UNIT_NAME = "persons";
  private EntityManagerFactory entityManagerFactory;

  public PersonRepository() {
    try {
      Properties dbProperties = new Properties();
      dbProperties.load(this.getClass().getClassLoader().getResourceAsStream("persistence.properties"));
      entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME,dbProperties);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Person add(Person person) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    entityManager.getTransaction().begin();
    entityManager.persist(person);
    entityManager.getTransaction().commit();
    return person;
  }

  public Person get(String name) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    TypedQuery<Person> query = entityManager.createQuery(
        "select u from Person u where u.name=:name", Person.class);
    query.setParameter("name", name);
    Person u = query.getSingleResult();
    return u;
  }
}
