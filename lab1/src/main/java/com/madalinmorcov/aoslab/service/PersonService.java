package com.madalinmorcov.aoslab.service;

import com.madalinmorcov.aoslab.persistence.repositories.PersonRepoFactory;
import com.madalinmorcov.aoslab.persistence.entities.Person;
import com.madalinmorcov.aoslab.persistence.repositories.PersonRepository;

public class PersonService {

  public void add(Person person){
    PersonRepository repository = PersonRepoFactory.getPersonRepo();
    repository.add(person);
  }

  public void add(String name, String email){
    Person person=new Person(name,email);
    add(person);
  }
}
