CREATE schema lab5;

CREATE TABLE persons (
    id int NOT NULL  AUTO_INCREMENT,
    email varchar(255),
    first_name varchar(255),
    last_name varchar(255),
    PRIMARY KEY (id)
);

INSERT INTO persons (email, first_name, last_name)
VALUES ('madalinmorcov@gmail.com','Madalin','Morcov');