package com.madalinmorcov.aoslab.lab5.service;

import com.madalinmorcov.aoslab.lab5.persistence.entities.Person;
import com.madalinmorcov.aoslab.lab5.persistence.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("personService")
@Transactional
public class PersonServiceImpl implements PersonService {

  @Autowired
  private PersonRepository repository;

  @Override
  public Person findById(int id) {
    return repository.findById(id).orElse(null);
  }

  @Override
  public Person findByEmail(String email) {
    return repository.findByEmail(email).orElse(null);
  }

  @Override
  public void savePerson(Person person) {
    repository.save(person);
  }

  @Override
  public void updatePerson(Person person) {
    repository.save(person);
  }

  @Override
  public void deletePersonById(int id) {
    repository.deleteById(id);
  }

  @Override
  public List<Person> findAllPersons() {
    return (List<Person>) repository.findAll();
  }

  @Override
  public void deleteAllPersons() {
    repository.deleteAll();
  }

  @Override
  public boolean personExists(Person person) {
    return repository.findByEmail(person.getEmail()).orElse(null) != null;
  }
}
