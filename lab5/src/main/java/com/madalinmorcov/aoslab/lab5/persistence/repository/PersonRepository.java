package com.madalinmorcov.aoslab.lab5.persistence.repository;

import com.madalinmorcov.aoslab.lab5.persistence.entities.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PersonRepository extends CrudRepository<Person,Integer> {
  Optional<Person> findByEmail(String email);
}
