package com.madalinmorcov.aoslab.lab5.controller;

import com.madalinmorcov.aoslab.lab5.persistence.entities.Person;
import com.madalinmorcov.aoslab.lab5.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
public class PersonsController {

  @Autowired
  PersonService personService;  //Service which will do all data retrieval/manipulation work


  //-------------------Retrieve All Persons--------------------------------------------------------

  @RequestMapping(value = "/person/", method = RequestMethod.GET)
  public ResponseEntity<List<Person>> listAllPersons() {
    List<Person> persons = personService.findAllPersons();
    if(persons.isEmpty()){
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return new ResponseEntity<>(persons, HttpStatus.OK);
  }


  //-------------------Retrieve Single Person--------------------------------------------------------

  @RequestMapping(value = "/person/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Person> getPerson(@PathVariable("id") int id) {
    System.out.println("Fetching Person with id " + id);
    Person person = personService.findById(id);
    if (person == null) {
      System.out.println("Person with id " + id + " not found");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(person, HttpStatus.OK);
  }



  //-------------------Create a Person--------------------------------------------------------

  @RequestMapping(value = "/person/", method = RequestMethod.POST)
  public ResponseEntity<Void> createPerson(@RequestBody Person person, UriComponentsBuilder ucBuilder) {
    System.out.println("Creating Person with email:" + person.getEmail());

    if (personService.personExists(person)) {
      System.out.println("A Person with email: " + person.getEmail() + " already exist");
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    personService.savePerson(person);

    HttpHeaders headers = new HttpHeaders();
    headers.setLocation(ucBuilder.path("/person/{id}").buildAndExpand(person.getId()).toUri());
    return new ResponseEntity<>(headers, HttpStatus.CREATED);
  }


  //------------------- Update a Person --------------------------------------------------------

  @RequestMapping(value = "/person/{id}", method = RequestMethod.PUT)
  public ResponseEntity<Person> updatePerson(@PathVariable("id") int id, @RequestBody Person person) {
    System.out.println("Updating Person " + id);

    Person currentPerson = personService.findById(id);

    if (currentPerson==null) {
      System.out.println("Person with id " + id + " not found");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    currentPerson.setEmail(person.getEmail());
    currentPerson.setFirstName(person.getFirstName());
    currentPerson.setLastName(person.getLastName());

    personService.updatePerson(currentPerson);
    return new ResponseEntity<>(currentPerson, HttpStatus.OK);
  }

  //------------------- Delete a Person --------------------------------------------------------

  @RequestMapping(value = "/person/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<Person> deletePerson(@PathVariable("id") int id) {
    System.out.println("Fetching & Deleting Person with id " + id);

    Person person = personService.findById(id);
    if (person == null) {
      System.out.println("Unable to delete. Person with id " + id + " not found");
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    personService.deletePersonById(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }


  //------------------- Delete All Persons --------------------------------------------------------

  @RequestMapping(value = "/person/", method = RequestMethod.DELETE)
  public ResponseEntity<Person> deleteAllPersons() {
    System.out.println("Deleting All Persons");

    personService.deleteAllPersons();
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
