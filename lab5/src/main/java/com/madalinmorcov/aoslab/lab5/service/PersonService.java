package com.madalinmorcov.aoslab.lab5.service;

import com.madalinmorcov.aoslab.lab5.persistence.entities.Person;

import java.util.List;

public interface PersonService {
  Person findById(int id);

  Person findByEmail(String email);

  void savePerson(Person user);

  void updatePerson(Person user);

  void deletePersonById(int id);

  List<Person> findAllPersons();

  void deleteAllPersons();

  boolean personExists(Person person);
}
